# Assignment Komputer Store App

# Link Heroku: https://vue-noroff-quiz.herokuapp.com/

## Table of Contants
- [Install](#install)
- [Usage](#usage)
- [Description](#description)
- [Maintainers](#maintainers)
- [Visuals](#visuals)
- [Project status](#project-status)

## Install

##### Clone ripository from git

    git clone git@gitlab.com:assigment2-team2/trivia-game.git

##### Then install

    npm install
    npm install he

## Usage

    npm run dev

## Description
An online trivia game as a Single Page Application using the Vue.js framework (version 3.x)

Trivia Game are a quiz game that test your knowledge on a wide range of trivial topics. In this application you will be able create an user account then choose number of questions, category and difficulty for the game.

### User API
- Store infomation about user -> 'username' and 'highscore'/'score' 

### Question API
- Store questions and information -> 'numberOfQuestons', 'category', 'difficulty'

### StartScreen
- View Start screen where user logs in with user name and select number of questions, category and difficulty for the Trivia quiz

### QuestionScreen
- Views the questions for the game, selected based on information given by the user on Startscreen

### ResultScreeen
- Displays the result and the users hisgscore/score

## Maintainers
Esben Bjarnason - @EsbenLB

Silja Stubhag Torkildsen - @SiljaTorki


## Visuals

### Component tree:

![Component tree](images/tree.png)


## Project status


Not completed:
-  user can click anywhere on the screen to start playing
