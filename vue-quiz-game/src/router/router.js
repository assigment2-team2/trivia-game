import { createRouter, createWebHistory } from 'vue-router'
import StartScreen from "../components/Screen/StartScreen.vue"
import QuestionScreen from "../components/Screen/QuestionScreen.vue"
import ResultScreen from "../components/Screen/ResultScreen.vue"

const routes = [{
        path: '/',
        component: StartScreen
    },
    {
        path: '/questionscreen',
        component: QuestionScreen
    },
    {
        path: '/resultscreen',
        component: ResultScreen
    },
    // {
    //     path: "/Trivia",
    //     component: Trivia
    // }

]


export default createRouter({
    history: createWebHistory(),
    routes,
})