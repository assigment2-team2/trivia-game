import { createStore } from 'vuex'
import { QuestionsAPI } from "../components/API/QuestionsAPI"
import { UserAPI } from "../components/API/UserAPI"
import he from 'he'

export default createStore({
    state: {
        questions: [],
        question: [],
        username: '',
        highScore: 0,
        categories: [],
        questionIndex: 0,
        maxNrOfQuestions: 0,
        userAnswers: [],
        questionLinkOption: ''
    },
    // change variables
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setQuestion: (state, payload) => {
            state.question = payload
        },
        setQuestionIndex: (state, payload) => {
            state.questionIndex = payload
        },
        setMaxNrOfQuestions: (state, payload) => {
            state.maxNrOfQuestions = payload
        },
        setUsername: (state, payload) => {
            state.username = payload;
        },
        setUserAnswers: (state, payload) => {
            state.userAnswers.push(payload)
        },
        setHighScore: (state) => {
            state.highScore = state.highScore + 10
        },
        setScoreAndAnswersToZero: (state) => {
            state.userAnswers = []
            state.highScore = 0
        },
        setQuestionLinkOption: (state, payload) => {
            state.questionLinkOption = payload;
        }
    },
    // Get variable
    getters: {
        getAllQuestion: (state) => {
            return state.questions
        },
        // return one question
        getOneQuestion: (state) => {
            return {
                question: state.question.question,
                correct_answer: state.question.correct_answer,
                answers: state.question.incorrect_answers
            }
        },
        getAllAnswers: (state) => {
            return state.userAnswers
        },
        getHighScore: (state) => {
            return state.highScore
        },
        getQuestionIndex: (state) => {
            return state.questionIndex
        },
        getMaxNrOfQuestions: (state) => {
            return state.maxNrOfQuestions
        },
        getLink: (state) => {
            return state.linkLinkOption
        },
    },
    actions: {
        // Get question api response, add id and shuffle questions
        async fetchQuestions({ commit }) {
            const questions = await QuestionsAPI.fetchQuestions(this.state.questionLinkOption)
                // for each question in questions
            questions.forEach(item => {
                    // Remove weird symbols from text | decode api response
                    item.question = he.decode(item.question)
                        // add correct answer to incorrect_answer. "Becomes" alle answer
                    item.incorrect_answers.unshift(item.correct_answer)
                        // Give all element id's, right answer always "0"
                    item.incorrect_answers.forEach((object, i) => {
                        item.incorrect_answers[i] = { id: i, text: object }
                            // Remove weird symbols from text | decode api response
                        item.incorrect_answers[i].text = he.decode(item.incorrect_answers[i].text)

                    });

                    // Shuffle answers
                    item.incorrect_answers = item.incorrect_answers
                        .map((value) => ({ value, sort: Math.random() }))
                        .sort((a, b) => a.sort - b.sort)
                        .map(({ value }) => value)
                })
                // Set all variables
            commit('setMaxNrOfQuestions', questions.length)
            commit('setQuestionIndex', 0)
            commit('setQuestions', questions)
                // Add first question
            commit('setQuestion', this.state.questions[this.state.questionIndex])
            commit('setQuestionIndex', (this.state.questionIndex + 1))
                // Reset Score and Answers
            commit('setScoreAndAnswersToZero')
        },

        // Update curQuestion and curIndex
        nexQuestion({ commit }) {
            commit('setQuestion', this.state.questions[this.state.questionIndex])
            commit('setQuestionIndex', (this.state.questionIndex + 1))
        },
        // Reset current question
        resetAll({ commit }) {
            commit('setQuestion', [])
        },
        // Check has user, if not add user
        async loginUser({ commit }, payload = "") {
            const response = await UserAPI.getUserById(payload)

            commit('setUsername', payload)
            if (response.length == 1) {
                // user exist, all good
            } else {
                // No user in DB, add user
                await UserAPI.createUser(payload)
            }
        },
        // Update highScore, if current score is bigger then highScore
        async saveHighScore() {
            await UserAPI.updatedUser(this.state.username, this.state.highScore)
        }
    }
})