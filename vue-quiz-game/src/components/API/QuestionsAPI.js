// Fetch all Questions
export const QuestionsAPI = {
    fetchQuestions(link = "") {
        return fetch(link)
            .then(response => response.json())
            .then(data => data.results)
    }
}