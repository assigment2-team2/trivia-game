export const UserAPI = {

    //Fetch user from API
    async getUserById(username) {
        return fetch("https://noroff-assignment-api-heroku.herokuapp.com/trivia?username=" + username)
            .then(response => response.json())
            // .then(results => {
            //     //if(results.length == 0)
            // })
            // .catch(error => { console.log(err) })
    },

    //Create new User
    async createUser(username) {
        const apiURL = "https://noroff-assignment-api-heroku.herokuapp.com"
        const apiKey = "NsJVQt2kKkmc5z2EC9EUhPxD9ZKb5TcipTgzqkk8jHmcNgenTRVAjpWvF8HvSGS2"

        // make user fetch(apiLink, Headers, body)
        return fetch(`${apiURL}/trivia`, {
                method: 'POST',
                headers: {
                    'X-API-Key': apiKey,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: username,
                    highScore: 0
                })
            })
            .then(response => {
                //check response, if not ok -> error
                if (!response.ok) {
                    throw new Error('Could not create new user')
                }
                // respons ok - return response JSON format
                return response.json()
            })
            .then(newUser => {
                // newUser is the new user with an id
            })
            .catch(error => {})
    },

    //Update User
    async updatedUser(username, newHighScore) {
        //Get user to update
        let player = await this.getUserById(username)

        if (player.length == 1 && typeof player[0].highScore !== 'undefined' && player.highScore !== null && player[0].highScore < newHighScore) {
            const userId = player[0].id // Update user with id 1
            const apiURL = 'https://noroff-assignment-api-heroku.herokuapp.com' //API url
            const apiKey = 'NsJVQt2kKkmc5z2EC9EUhPxD9ZKb5TcipTgzqkk8jHmcNgenTRVAjpWvF8HvSGS2' //API key

            fetch(`${apiURL}/trivia/${userId}`, {
                    method: 'PATCH', // PATCH -> method is used to modify the values of the resource properties
                    headers: {
                        'X-API-Key': apiKey,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        // Provide new highScore to add to user with id 1
                        highScore: newHighScore
                    })
                })
                .then(response => {
                    //check response, if not ok -> error
                    if (!response.ok) {
                        console.log("Error")
                        throw new Error('Could not update high score')

                    }
                    // respons ok - return response JSON format
                    return response.json()
                })
                .then(updatedUser => {
                    // updatedUser is the user with the Patched data
                })
                .catch(error => {})
        } else {}


    }
}